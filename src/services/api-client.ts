import axios from "axios";

export default axios.create({
  baseURL: "https://api.rawg.io/api",
  params: {
    key: "52253ad451a445d0989eddfa1b3454e8",
  },
});